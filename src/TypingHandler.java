import com.intellij.codeInsight.template.impl.editorActions.TypedActionHandlerBase;
import com.intellij.openapi.actionSystem.DataContext;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.editor.actionSystem.TypedActionHandler;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class TypingHandler extends TypedActionHandlerBase {
    final private GenericHandler genericHandler;

    public TypingHandler(@Nullable TypedActionHandler originalHandler) {
        super(originalHandler);
        genericHandler = new GenericHandler();
    }

    @Override
    public void execute(@NotNull Editor editor, char charTyped, @NotNull DataContext dataContext) {
        genericHandler.execute(
            editor,
            () -> {
                if (myOriginalHandler != null) {
                    myOriginalHandler.execute(editor, charTyped, dataContext);
                }
            }
        );
    }
}
