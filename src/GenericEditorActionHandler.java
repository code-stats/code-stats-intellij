import com.intellij.openapi.actionSystem.DataContext;
import com.intellij.openapi.editor.Caret;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.editor.actionSystem.EditorActionHandler;
import com.intellij.openapi.editor.actionSystem.EditorWriteActionHandler;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class GenericEditorActionHandler extends EditorWriteActionHandler {
    final private GenericHandler genericHandler;
    final private EditorActionHandler myEditorActionHandler;
    private int caretsToIgnore = 0;

    public GenericEditorActionHandler(EditorActionHandler editorActionHandler) {
        super(true);
        genericHandler = new GenericHandler();
        myEditorActionHandler = editorActionHandler;
    }

    @Override
    public void executeWriteAction(@NotNull Editor editor, @Nullable Caret caret, DataContext dataContext) {
        caretsToIgnore = genericHandler.execute(
                caretsToIgnore,
                editor,
                () -> myEditorActionHandler.execute(editor, caret, dataContext)
        );
    }
}
