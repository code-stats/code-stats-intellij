import com.intellij.openapi.diagnostic.Logger;

public class CodeStatsLogger {
    private static final Logger LOG = Logger.getInstance(CodeStatsLogger.class);

    public static void debug(String msg) {
        if (LOG.isDebugEnabled()) {
            LOG.debug(msg);
        }
    }

    public static void info(String msg) {
        LOG.info(msg);
    }

    public static void warn(String msg) {
        LOG.warn(msg);
    }

    public static void error(String msg) {
        LOG.error(msg);
    }
}
